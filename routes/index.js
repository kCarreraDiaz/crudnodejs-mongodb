const express = require('express');
const router = express.Router();
//const path = require('path');
const Producto = require('../models/producto');

router.get('/landing', async (req, res) => {
    const productos = await Producto.find();
    console.log(productos);
    res.render('landing', {
        productos
    });
});

// usamos AWAIT para ejecutar consultas de BD
router.get('/', async (req, res) => {
    const productos = await Producto.find();
    console.log(productos);
    res.render('index', {
        productos
    });
});

router.post('/add', async (req, res) => {
    //console.log(new Producto(req.body));// creo objeto para almacenar en BD
    //res.send('recibido');
    const producto = new Producto(req.body);
    await producto.save(); //guardo en la BD
    //res.send('recibido');
    res.redirect('/');
});

router.get('/convertir/:id', async (req, res) => {//cambiar estado (true, false)
    const { id } = req.params;
    const producto = await Producto.findById(id);
    producto.estado = !producto.estado;
    await producto.save();
    console.log("recibido");
    res.redirect('/');
});

router.get('/show/:id', async (req, res) => {// show
    const { id } = req.params;
    const producto = await Producto.findById(id);
    res.render('show', {
        producto
    });
});

router.get('/edit/:id', async (req, res) => {//cambiar estado (true, false)
    const { id } = req.params;
    const producto = await Producto.findById(id);
    res.render('edit', {
        producto
    });
});

router.post('/edit/:id', async (req, res) => {
    const { id } = req.params;
    await Producto.update({_id: id}, req.body); //await (tarea de BD)
    res.redirect('/');
})

router.get('/delete/:id', async (req, res) => {
    const { id } = req.params;
    await Producto.remove({_id: id});
    res.redirect('/');
});
module.exports = router;
