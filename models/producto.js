const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ProductoSchema = new Schema ({
        nombre: { type: String },
        description: { type: String},
        precio : { type: String },
        stock : { type: String },
        estado : { type: String},
});

module.exports = mongoose.model('productos', ProductoSchema)
