const path = require('path');
const express = require('express');
const morgan = require('morgan');
const db = require('./db');

const app = express();


// connecting to db
//mongoose.connect('mongodb://localhost/crud-mongo');   esta en el archivo db.js


// Importando routes
const indexRoutes = require('./routes/index');

// Settings
//app.set('port', process.env.PORT || 3000); declaramos variable para el puerto
app.set('views', path.join(__dirname, 'views'));// ruta para renderizar
app.set('view engine', 'ejs');

// middlewares (se ejecutan antes de que llegan a las rutas)
app.use(morgan('dev'));
app.use(express.urlencoded({extended: false}));//para entender los datos del formulario (false: xq solo enviamos texto)
// routes
app.use('/', indexRoutes);
// starting the server
const port = 3000;

app.listen(port, function () {
  console.log('Example app listening on port 3000')
})
